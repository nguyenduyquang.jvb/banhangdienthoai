<?php
namespace Admin\Model\Table;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class UsersTable extends Table
{

    public function validationDefault(Validator $validator)
    {
        return $validator
            ->notEmpty('email', 'A username is required')
            ->notEmpty('password', 'A password is required')
            ->notEmpty('level', 'A level is required')
            ->notEmpty('telephone', 'A telephone is required')
            ->add('level', 'inList', [
                'rule' => ['inList', [1,2]],
                'message' => 'Please enter a valid level'
            ]);
    }

}
?>