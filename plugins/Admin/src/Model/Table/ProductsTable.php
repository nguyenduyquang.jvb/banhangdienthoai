<?php
namespace Admin\Model\Table;
use Cake\ORM\Table;
use Cake\Validation\Validator;

class ProductsTable extends Table
{

    public function initialize(array $config)
    {
        $this->setTable('products');
        $this->setDisplayField('prod_name');
        $this->setPrimaryKey('prod_id');
        
    }

}
?>