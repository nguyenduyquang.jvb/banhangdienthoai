<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<title>Untitled Document</title>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
<style>
#navbar{
	margin-top:50px;}
#tbl-first-row{
	font-weight:bold;}
#logout{
	padding-right:30px;}		
</style>
</head>
<body>

<div class="container">
    <div id="navbar" class="row">
    	<div class="col-sm-12">
        	<nav class="navbar navbar-default">
  				<div class="container-fluid">
                	<ul class="nav navbar-nav">
                        <li><?= $this->Html->link(__('Home'), ['action' => 'index']) ?></li>
                        <li><?= $this->Html->link(__('Users'), ['action' => 'user']) ?></li>
                        <li><?= $this->Html->link(__('Add User'), ['action' => 'add']) ?></li>
                	</ul>
                    <p id="logout" class="navbar-text navbar-right"><a class="navbar-link" href="#">Logout</a></p>
                </div>
            </nav>
        </div>
    </div>
    <div class="row">
    	<div class="col-sm-12">
        	<table class="table table-striped">
            	<tr id="tbl-first-row">
                	<td width="5%">#</td>
                    <td width="25%">Email</td>
                    <td width="25%">Password</td>
                    <td width="10%">Level</td>
                    <td width="15%">Telephone</td>
                    <td width="10%">Edit</td>
                    <td width="10%">Delete</td>
                </tr>
                <?php foreach($user as $username): ?>
                <tr>
                	<td><?php echo ($username->id); ?></td>
                    <td><?php echo ($username->email); ?></td>
                    <td><?php echo ($username->password); ?></td>
                    <td><?php echo ($username->level); ?></td>
                    <td><?php echo ($username->telephone); ?></td>
                    <td><?php echo $this->Html->link('Edit', 
                        ['controller' => 'Users', 'action' => 'edit', $username->id]); ?>
                    </td>
                    <td><?php echo $this->Form->postLink('Delete',
                        ['controller' => 'Users', 'action' => 'delete', $username->id],
                        ['confirm','Bạn có muốn xóa?']); ?>
                    </td>
                </tr>
                <?php endforeach; ?>
			</table>
            <div aria-label="Page navigation">
            	<ul class="pagination">
                    <li><?php echo $this->Paginator->prev(' << ' . __('Previous')); ?> </li>
                    <li><?php echo $this->Paginator->numbers(); ?> </li>
                    <li><?php echo $this->Paginator->next(' >> ' . __('Next')); ?> </li>
                </ul>
            </div>
        </div>
    </div>
</div>

</body>
</html>
