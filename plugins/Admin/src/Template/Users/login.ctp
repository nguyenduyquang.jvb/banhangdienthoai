<!DOCTYPE html>
<html>
<head>
	<title>Login</title>
</head>
<body>
<div class="users form">
	<?php echo $this->Flash->render(); ?>
	<?php echo $this->Form->create(); ?>
		<fieldset>
			<legend><?php echo ('Hãy nhập email và mật khẩu.'); ?></legend>
			<?php echo $this->Form->control('email'); ?>
			<?php echo $this->Form->control('password'); ?>
		</fieldset>
		<?php echo $this->Form->button(__('Login')); ?>
		<?php echo $this->Form->end(); ?>
</div>
</body>
</html>