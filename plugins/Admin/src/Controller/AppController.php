<?php

/**
 * CakePHP(tm) : Rapid Development Framework (https://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (https://cakefoundation.org)
 * @link      https://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   https://opensource.org/licenses/mit-license.php MIT License
 */

namespace Admin\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use App\Controller\AppController as BaseController;

class AppController extends BaseController {

    public function initialize(){
        parent::initialize();

        $this->loadComponent('RequestHandler', ['enableBeforeRedirect' => false]);
        $this->loadComponent('Flash');
        $this->loadComponent('Auth', [
        'loginRedirect' => [           
            'controller' => 'Users',
            'action' => 'user'
        ],
        'logoutRedirect' => [
            'controller' => 'Users',
            'action' => 'login'
        ]
    ]);
    }

    public function beforeRender(Event $event){
        $this->set('Auth', $this->Auth);
    }

    public function isAuthorized($used){
        if(isset($user['level']) && $user['level'] === 1){
            return true;
        }
        return false;
    }

}
