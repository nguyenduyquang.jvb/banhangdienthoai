<?php

namespace Admin\Controller;

use Admin\Controller\AppController;
use Cake\Event\Event;

class ProductsController extends AppController {

    public function initialize() {
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth');
        //$this->loadComponent('uploadFile');
        //$this->loadComponent('uploadPath');
    }

    public function beforeFilter(Event $event) {
        parent::beforeFilter($event);

        $this->Auth->allow(['product', 'addproduct', 'editproduct', 'deleteproduct', 'logout']);
    }

    public function product() {
        $list_prod = $this->Products->find('all');
        $prod = $this->Paginator->paginate($list_prod, ['limit' => 5]);
        $this->set(compact('prod'));
        $this->set('serialize', ['prod']);
    }

    public function addproduct() {
        $prod = $this->Products->newEntity();
        if ($this->request->is('post')) {
            //Đoạn mới thêm vào
            $data = $this->request->getData();
            if (!empty($data['prod_img']['name'])) {
                $fileName = $data['prod_img']['name'];
                $uploadPath = 'img/';
                $uploadFile = $uploadPath . $fileName;
                if (move_uploaded_file($data['prod_img']['tmp_name'], $uploadFile)) {
                    $data['prod_img'] = $fileName;
                }
            }
            $this->Products->patchEntity($prod, $data);
            if ($this->Products->save($prod)) {
                $this->Flash->success(__('Thành viên đã được lưu.'));
                return $this->redirect(['action' => 'product']);
            }
            $this->Flash->error(__('Không lưu được thành viên.'));
        }
        $this->set('prod', $prod);
    }

}
