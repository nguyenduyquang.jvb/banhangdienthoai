<?php
namespace Admin\Controller;
use Admin\Controller\AppController;
use Cake\Event\Event;

class UsersController extends AppController {

    public function initialize() {
        parent::initialize();

        $this->loadComponent('Paginator');
        $this->loadComponent('Flash');
        $this->loadComponent('Auth');
    }

    public function beforeFilter(Event $event){
        parent::beforeFilter($event);

        $this->Auth->allow(['user','logout','add','edit','delete']);
    }

    public function login(){
        if($this->request->is('post')){
            $user = $this->Auth->identify();
            if($user){
                $this->Auth->setUser($user);
                return $this->redirect($this->Auth->redirectUrl());
            }
            $this->Flash->error(__('Sai email hoặc mật khẩu.'));
        }
    }

    public function logout(){
        $this->Flash->success(__('Bạn đã đăng xuất.'));
        return $this->redirect($this->Auth->logout());
    }

    public function index(){
       
    }

    public function user(){
    	$list_user = $this->Users->find('all');
        $user = $this->Paginator->paginate($list_user, ['limit' => 5]);
        $this->set(compact('user'));
        $this->set('serialize', ['user']);
    }

    public function add(){
        $user = $this->Users->newEntity();
        if ($this->request->is('post')) {
            $this->Users->patchEntity($user, $this->request->getData());
            if ($this->Users->save($user)) {
                $this->Flash->success(__('Thành viên đã được lưu.'));
                return $this->redirect(['action' => 'user']);
            }
            $this->Flash->error(__('Không lưu được thành viên.'));
        }
        $this->set('user', $user);
    }

    public function edit($id) {
    	$user = $this->Users->get($id);
    	if($this->request->is(['post','put'])){
    		$this->Users->patchEntity($user, $this->request->getData());
    		if($this->Users->save($user)){
    			$this->Flash->success(__('Bạn đã sửa thành công.'));
    			return $this->redirect(['action' => 'user']);
    		}
    		$this->Flash->error(__('Chưa sửa thành công.'));
    	}
    	$this->set('user', $user);
    }

    public function delete($id){
        $this->request->allowMethod(['post','delete']);
        $user = $this->Users->findById($id)->firstOrFail();
        if($this->Users->delete($user)){
            $this->Flash->success(__('Bạn đã xóa {0} thành công.',$user->email));
            return $this->redirect(['action' => 'user']);
        }

    }

}
