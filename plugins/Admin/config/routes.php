<?php

use Cake\Routing\RouteBuilder;
use Cake\Routing\Router;
use Cake\Routing\Route\DashedRoute;

Router::plugin(
    'Admin', ['path' => '/admin'], function ($routes){
    $routes->connect('/users', ['controller' => 'Users', 'action' => 'index']);
    $routes->connect('/users/login', ['controller' => 'Users', 'action' => 'login']);
    $routes->connect('/users/user', ['controller' => 'Users', 'action' => 'user']);
    $routes->connect('/users/add', ['controller' => 'Users', 'action' => 'add']);
    $routes->connect('/users/edit/:id', ['controller' => 'Users', 'action' => 'edit'], ['id' => '\d+', 'pass' => ['id']]);
    $routes->connect('/users/delete/:id', ['controller' => 'Users', 'action' => 'delete'], ['id' => '\d+', 'pass' => ['id']]);

    $routes->connect('/products/product', ['controller' => 'Products', 'action' => 'product']);
    $routes->connect('/products/addproduct', ['controller' => 'Products', 'action' => 'addproduct']);
    $routes->connect('/products/editproduct/:id', ['controller' => 'Products', 'action' => 'editproduct'], ['id' => '\d+', 'pass' => ['id']]);
    $routes->connect('/products/deleteproduct', ['controller' => 'Products', 'action' => 'deleteproduct'], ['id' => '\d+', 'pass' => ['id']]);
});
