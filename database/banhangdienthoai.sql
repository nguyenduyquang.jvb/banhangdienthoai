-- phpMyAdmin SQL Dump
-- version 4.8.0
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 15, 2018 at 11:48 AM
-- Server version: 10.1.31-MariaDB
-- PHP Version: 7.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `banhangdienthoai`
--

-- --------------------------------------------------------

--
-- Table structure for table `bills`
--

CREATE TABLE `bills` (
  `bill_id` int(12) NOT NULL,
  `bill_number` int(12) NOT NULL,
  `bill_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `bill_telephone` int(12) NOT NULL,
  `bill_email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `bill_address` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `cart_id` int(12) NOT NULL,
  `cart_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `cart_price` int(12) NOT NULL,
  `cart_number` int(3) NOT NULL,
  `cart_bill` int(12) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `cate_id` int(12) NOT NULL,
  `cate_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `com_id` int(12) NOT NULL,
  `com_email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `com_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `com_content` varchar(255) CHARACTER SET utf8 NOT NULL,
  `com_product` int(12) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `products`
--

CREATE TABLE `products` (
  `prod_id` int(12) NOT NULL,
  `prod_name` varchar(255) CHARACTER SET utf8 NOT NULL,
  `prod_price` int(12) NOT NULL,
  `prod_img` varchar(255) CHARACTER SET utf8 NOT NULL,
  `prod_warranty` varchar(255) CHARACTER SET utf8 NOT NULL,
  `prod_accessories` varchar(255) CHARACTER SET utf8 NOT NULL,
  `prod_condition` varchar(255) CHARACTER SET utf8 NOT NULL,
  `prod_promotion` varchar(255) CHARACTER SET utf8 NOT NULL,
  `prod_status` int(3) NOT NULL,
  `prod_description` varchar(255) CHARACTER SET utf8 NOT NULL,
  `prod_featured` int(3) NOT NULL,
  `prod_cate` int(12) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(12) NOT NULL,
  `email` varchar(255) CHARACTER SET utf8 NOT NULL,
  `password` varchar(255) CHARACTER SET utf8 NOT NULL,
  `level` int(3) NOT NULL,
  `telephone` int(12) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `email`, `password`, `level`, `telephone`, `created_at`, `updated_at`) VALUES
(1, 'nguyenvana@gmail.com', '123456', 1, 123456789, '2018-05-11 01:15:30', NULL),
(2, 'nguyenvanb@gmail.com', '123456', 2, 123456789, '2018-05-11 03:00:05', NULL),
(3, 'nguyenvanc@gmail.com', '123456', 2, 123456789, '2018-05-11 03:00:58', NULL),
(4, 'nguyenvand@gmail.com', '123456', 2, 123456789, '2018-05-14 16:27:28', NULL),
(5, 'nguyenvane@gmail.com', '123456', 2, 123456789, '2018-05-15 07:40:33', NULL),
(6, 'nguyenvanf@gmail.com', '123456', 2, 123454456, '2018-05-15 07:42:49', NULL),
(7, 'nguyenvang@gmail.com', '123456', 2, 123343123, '2018-05-15 07:45:15', NULL),
(8, 'nguyenvanh@gmail.com', '123456', 2, 1993229293, '2018-05-15 07:56:43', NULL),
(9, 'nguyenvanl@gmail.com', '123456', 2, 293848764, '2018-05-15 08:05:17', NULL),
(10, 'nguyenvank@gmail.com', '123456', 2, 1293884875, '2018-05-15 08:08:13', NULL),
(11, 'nguyenvank@gmail.com', '123456', 2, 1293884875, '2018-05-15 08:08:13', NULL);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bills`
--
ALTER TABLE `bills`
  ADD PRIMARY KEY (`bill_id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`cart_id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`cate_id`);

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`com_id`);

--
-- Indexes for table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`prod_id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bills`
--
ALTER TABLE `bills`
  MODIFY `bill_id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `cart_id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `cate_id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `com_id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `products`
--
ALTER TABLE `products`
  MODIFY `prod_id` int(12) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(12) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
